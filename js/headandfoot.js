$(function() {
    $(".erweima").mousemove(function() {
        $(".zn_heardererweima").css("display", "block");
    });
    $(".erweima").mouseleave(function() {
        $(".zn_heardererweima").css("display", "none");
    });


    $(".financialbbutton").change(function() {
        //					获取文件名
        //					var path=$(this).val();
        //              		var    path1 = path.lastIndexOf("\\");
        //              		var name = path.substring(path1+1);
        //$(".tximg").attr("src", $(".touxiang").val());
        if (checkPic("financialbbutton")) {
            $(".financialbimg").attr("src", preImg(this.id, 'financialbimg'));
        }

    })

    $(".fusertouxiang_btn").change(function() {
        //					获取文件名
        //					var path=$(this).val();
        //              		var    path1 = path.lastIndexOf("\\");
        //              		var name = path.substring(path1+1);
        //$(".tximg").attr("src", $(".touxiang").val());
        if (checkPic("fusertouxiang_btn")) {
            $(".fusertouxiangimg").attr("src", preImg(this.id, 'fusertouxiangimg'));
        }

    })

    function checkPic(buttonsrc) {
        var picPath = $("." + buttonsrc).val()
        var type = picPath.substring(picPath.lastIndexOf(".") + 1, picPath.length).toLowerCase();
        if (type != "jpg" && type != "bmp" && type != "gif" && type != "png") {
            alert("请上传正确的图片格式");
            return false;
        }
        return true;
    }

    function preImg(sourceId, targetId) {
        if (typeof FileReader === 'undefined') {
            alert('Your browser does not support FileReader...');
            return;
        }
        var reader = new FileReader();

        reader.onload = function(e) {
            var img = document.getElementById(targetId);
            img.src = this.result;
            //						$(".aaa").html(img.src);
            return img.src;
        }
        reader.readAsDataURL(document.getElementById(sourceId).files[0]);
    }

    
    
    //registered.html注册页面获取短信按钮事件
    $(".zn_phonebutton").click(function() {
        $(this).attr("disabled", "disabled");
        $(this).addClass("zn_phonebuttonaction");
        var i = 60;
        var btn = setInterval(function() {
            $(".zn_phonebutton").attr("value", i + "秒后重新获取验证码");           
            if (i == 0) {
                clearInterval(btn);
                $(".zn_phonebutton").attr("disabled", false);
                $(".zn_phonebutton").removeClass("zn_phonebuttonaction");
                $(".zn_phonebutton").attr("value", "重新获取验证码");
            }
            i = i - 1;
        }, 1000);
    })
    
    
    //index.html
     $(".zn_indexblist li").mousemove(function() {
                    $(this).css("border-bottom", "2px solid #f96520");
                    $(this).children("a").css("color", "#f96520");
                })
                $(".zn_indexblist li").mouseleave(function() {
                    $(this).css("border-bottom", "0");
                    $(this).children("a").css("color", "#676767");
                })
                $(".zn_indexblist1 li").mousemove(function() {
                    $(this).children("a").css("color", "#f96520");
                })
                $(".zn_indexblist1 li").mouseleave(function() {
                    $(this).children("a").css("color", "#676767");
                })
                $('.carousel').carousel({
                    interval: 2000
                })
                var list = $(".zn_indexblist li a");
                list.each(function() {
                    //alert($(this).attr("href"));
                    $($(this).attr("href")).hide();
                    if ($(this).hasClass("selected")) {
                        $($(this).attr("href")).show();
                    }
                    $(this).mousemove(function(event) {
                        event.preventDefault();
                        if (!$(this).hasClass("selected")) {
                            list.each(function() {
                                $(this).removeClass("selected");
                                $($(this).attr("href")).hide();
                            });
                            $(this).addClass("selected");
                            $($(this).attr("href")).show();
                        }
                    });
                });
                

                
                 $(".themefund").show();
                $(".themebtn1").click(function(){
                    $(".themebtn2").removeClass("themebtnaction");
                    $(".themebtn1").addClass("themebtnaction");
                    $(".themefund").fadeIn(300);
                    $(".themestock").fadeOut(300);
                })
                $(".themebtn2").click(function(){
                    $(".themebtn1").removeClass("themebtnaction");
                    $(".themebtn2").addClass("themebtnaction");
                     $(".themefund").fadeOut(300);
                    $(".themestock").fadeIn(300);
                   
                })
                
                
                

})